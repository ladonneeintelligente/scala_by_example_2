package sdz

object Sdz_exercice3 {

  /*
      Créez les classes vides Lit, Chaise et Bureau.

    Créez une classe Chambre qui contient des instances des classes ci-dessus.

    Créez une instance de Chambre. Qu'affiche la console ? Pourquoi ?
   */

  class Lit
  class Chaise
  class Bureau

  class Chambre {
    var lit = new Lit
    var chaise = new Chaise
    var bureau = new Bureau
  }

  var maChambre: Chambre = new Chambre



}
