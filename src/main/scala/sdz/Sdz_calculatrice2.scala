package sdz

object Sdz_calculatrice2 extends App{

  class Calculatrice {
    def somme2(x:Int, y:Int):Int = x +y
    def somme3(x:Int, y:Int, z:Int):Int = somme2(x, somme2(y, z))
  }

  val c = new Calculatrice
  println(c.somme2(5,6))
  val x: Int = 5
  println(c.somme2(x, 2 * x))
  println(c.somme3(1, 8, x))

}
