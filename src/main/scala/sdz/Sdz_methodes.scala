package sdz

object Sdz_methodes {

  /* methodes en scala (ne pas appeler cela fonctions !)  */

//  class Zero {
//    var pseudo = "Anonyme"
//    var age = 0
//    var citation = "Aucune citation"
//    var affichage = "Pseudo: " + pseudo +" Age: " + age + " Citation: " + citation
//  }
//
//  val z: Zero = new Zero // ou val z = new Zero (inference)
//  z.affichage

//    z.age = 15
//    z.affichage

    //Donc c'est impossible de gérer l'affichage avec une variable, on doit utiliser une méthode.

//  class Zero {
//    var pseudo = "Anonyme"
//    var age = 0
//    var citation = "Aucune citation"
//
//    def affichage = "Pseudo: " + pseudo + " Age: " + age + " Citation: " + citation
//  }

//    class Zero {
//      var pseudo = "Anonyme"
//      var age = 0
//      var citation = "Aucune citation"
//
//      def affichage = "Pseudo: " + pseudo + " Age: " + age + " Citation: " + citation
//
//      def avancerAge: Unit = age += 1 // ou juste def avancerAge = age +=1
//    }

//  class Zero {
//          var pseudo = "Anonyme"
//          var age = 0
//          var citation = "Aucune citation"
//
//          def affichage = "Pseudo: " + pseudo + " Age: " + age + " Citation: " + citation
//
//          def avancerAge: Unit = age += 1 // ou juste def avancerAge = age +=1
//
//          def ajouterAge(n: Int) = age += n
//        }

  //notation infixe : o1 methode 0o2, sinoon autres notations o1.methode(o2) ou bien o1 methode(o2)
  // Elle est très importante parce que c'est grâce à elle qu'on peut écrire x + y au lieu de x.+(y).

}
