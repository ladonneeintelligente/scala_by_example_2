package sdz

object Boucles {

  class Boucles {
    def boucleWhile = {
      var x=3
      while (x < 6) x += 1
    }
  }

  val b = new Boucles
  b.boucleWhile

}
