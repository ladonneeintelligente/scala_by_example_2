package sdz

/* exemples du cours ici : http://sdz.tdct.org/sdz/apprenez-la-programmation-avec-scala.html#Premierspas */
/* Auteur : XVA 10 Dec 2020 */

object Sdz_classe0 {
//     utilisez Scala REPL appelez d'ici
//     class Zero
//
//     val x: Zero = new Zero
//
//      class Zero { var pseudo: java.lang.String = "Anonyme" }
//
//     val z: Zero = new Zero()
//
//     z.pseudo
//
//     z.pseudo = "Zozor"
//
//     val x: Zero = new Zero
//      val y: Zero = new Zero
  //    x.pseudo = "Til0u"
  //    y.pseudo = "iPoulet"

  //    class A
  //    class B { var a: A = new A }

  //    val b1 = new B
  //    val b2 = new B


  /*
  class Zero {
     |   var pseudo: java.lang.String = "Anonyme"
     |   var age: Int = 0
     |   var citation: java.lang.String = "Aucune citation"
     | }
   */

  /*
      val moi: Zero = new Zero
      moi.pseudo = "Einstein++"
       moi.age = 21
       moi.citation = "J'adore Scala <3"
   */

//  class Zero {
//    var pseudo: String = "Anonyme"
//    var age: Int = 0
//    var citation: String = "Aucune citation"
//  }

//  val moi: Zero = new Zero
//  moi.pseudo
//
//  moi.age = 21
//  moi.citation = "j'adore scala "

//  class Zero {
//    var pseudo: String = "Anonyme"
//    var age: Int = 0
//    var citation: String = "Aucune citation"
//    var unevariable = "Pseudo: " + this.pseudo
//    var unesecondevariable = "Pseudo court " + pseudo
//  }

  /* un bloc shift entree pour retour ligne dans bloc dans scala interactif REPL*/

//  {
//    val i: Int = 2
//    val j: Int = 4
//    i + j
//  }

  /* Inférence */

  /* inference ; */
//  val x: Int = 0 ; val y: Double = 2.3

  /* inference type variable */
//  val x = "Bonjour"
//  val z = new Zero











}
