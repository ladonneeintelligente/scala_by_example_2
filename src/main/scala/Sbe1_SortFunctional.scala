/*
fonction de tri ecrite en style programmation fonctionnelle
auteur: XVA
 */
object Sbe1_SortFunctional {
  def sort(xs: Array[Int]): Array[Int] = {
    if (xs.length <= 1) xs
    else {
      val pivot = xs(xs.length / 2)
      Array.concat(sort(xs filter (pivot >)),
        xs filter (pivot ==),
        sort(xs filter (pivot <)))
    }
  }

  def main(args: Array[String]): Unit = {
    val numbers = Array(1, 2, 30, 10, 40, 4)
    sort(numbers)
    var numstrings = numbers.mkString(" , ")
    println(numstrings)
  }

}
